import unittest
from unittest.mock import create_autospec, patch
from server.data.models import Category
from server.services import category_services


class CategoryService_Should(unittest.TestCase):

    def test_all_createListOfCategories_when_dataIsPresent(self):
        get_data_func = lambda q: [(1,'test', True),(2,'test2', False)]

        result = list(category_services.all(get_data_func))

        self.assertEqual(2,len(result))


    def test_all_createsListOfCategories_when_dataIsPresent_withPatch(self):
        with patch('server.services.category_services.database') as mock_db:
            mock_db.read_query.return_value = [(1, 'test', False), (2, 'test2', False)]
            result = list(category_services.all_public())

        self.assertEqual(2, len(result))


    @patch('server.services.category_services.database', autospec= True)
    def test_public_createListOfCategories_when_dataIsPresent_withPatch_v2(self, mock_db):
        mock_db.read_query.return_value = [(1, 'test', True), (2, 'test2', False)]

        result = list(category_services.all_public())

        self.assertEqual(2, len(result))
    

    def test_if_category_exist_with_data(self):
        get_data_func = lambda q, id: [(1,"test", True)]

        result = category_services.exist(1, get_data_func)
        excepted = True
        
        self.assertEqual(excepted, result)
    
    def test_if_category_exist_without_data(self):
        get_data_func = lambda q, id: [()]
        result = category_services.exist(1, get_data_func)
        excepted = False
        
        self.assertEqual(excepted, result)
    
    def test_if_category_is_created(self):
        generated_id = 1
        insert_data_func = lambda q, cat: generated_id
        result = category_services.create(Category(name="test"), insert_data_func)
        excepted = Category(id=generated_id, name="test")

        self.assertEqual(excepted, result)
    
    def test_if_category_make_private_True(self):
        update_data_func = lambda q, id: True

        result = category_services.make_private(1, update_data_func)
        expected = True
        
        self.assertEqual(expected, result)
    
    def test_if_category_make_public_False(self):
        update_data_func = lambda q, id: False

        result = category_services.make_public(1, update_data_func)
        expected = False
        
        self.assertEqual(expected, result)
    
    def test_if_category_is_locked_True(self):
        update_data_func = lambda q, id: True

        result = category_services.locked(1, update_data_func)
        expected = True
        
        self.assertEqual(expected, result)
    
    def test_if_category_is_unlocked_False(self):
        update_data_func = lambda q, id: False

        result = category_services.unlock(1, update_data_func)
        expected = False
        
        self.assertEqual(expected, result)