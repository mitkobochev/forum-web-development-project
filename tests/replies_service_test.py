import unittest
from server.data.models import Replies,UserVoteModel
from server.services import replies_service


class RepliesService_Should(unittest.TestCase):

    def test_all_createReply_when_dataIsPresent(self):
        get_data_func = lambda q: [
            (1, 'fake_text', '2022-10-10', 1, 8,1,False), (1, 'fake_text', '2022-11-10', 1, 9,2,True)]

        result = list(replies_service.all(None, get_data_func))

        self.assertEqual(2, len(result))

    def test_if_reply_exist_with_data(self):
        get_data_func = lambda r,id: [
            (1, 'fake_text', '2022-10-10', 1, 8,1,False), (1, 'fake_text', '2022-11-10', 1, 9,2)]

        result = replies_service.exists(1,get_data_func)
        excepted = True

        self.assertEqual(excepted, result)

    def test_if_reply_exist_without_data(self):
        get_data_func = lambda r,id: []

        result = replies_service.exists(1,get_data_func)
        excepted = False

        self.assertEqual(excepted, result)

    def test_if_reply_is_created(self):
        generated_id = 1
        insert_data_func = lambda q, rep: generated_id
        result = replies_service.create(Replies(id=1, text="fake_reply",creation_date="2022-10-10",votes=5,topic_id=1,user_id=8,is_best_reply=True),insert_data_func)
        excepted = Replies(id=1, text="fake_reply",creation_date="2022-10-10",votes=5,topic_id=1,user_id=8,is_best_reply=True)

        self.assertEqual(excepted, result)
    

    def test_if_vote_insert_whenNone(self):
        generated_id = 1
        insert_data_func = lambda q, rep: generated_id

        result = replies_service.insert_vote_if_None(UserVoteModel(user_id=8,reply_id=1,vote=1), insert_data_func)
        expected = UserVoteModel(vote_id=1,user_id=8,reply_id=1,vote=1)

        self.assertEqual(expected,result)
    
    def test_if_vote_return_by_user_id_and_reply_id(self):
        get_data_func = lambda u,r: [(1,8,1,1)]

        result = replies_service.get_vote_id(8,1,get_data_func)
        expected = UserVoteModel(vote_id=1,user_id=8,reply_id=1,vote=1)
        
        self.assertEqual(expected,result)