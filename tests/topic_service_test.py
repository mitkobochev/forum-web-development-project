import unittest
from server.data.models import Topics
from server.services import topics_services


class TopicService_Should(unittest.TestCase):

    def test_all_createTopics_when_dataIsPresent(self):
        get_data_func = lambda q:  [
            (1, 'title', 'test desc', '2022-10-10', 8, 1), (2, 'title', 'test desc', '2022-10-10', 9, 3)]

        result = list(topics_services.all(None, get_data_func))

        self.assertEqual(2, len(result))

    def test_if_topic_exist_with_data(self):
        get_data_func = lambda q, id: [
            (1, 'title', 'test desc', '2022-10-10', 8, 1)]

        result = topics_services.exist(1, get_data_func)
        excepted = True

        self.assertEqual(excepted, result)

    def test_if_topic_exist_without_data(self):
        get_data_func = lambda q, id: []

        result = topics_services.exist(1, get_data_func)
        excepted = False

        self.assertEqual(excepted, result)

    def test_if_topic_is_created(self):
        generated_id = 1
        insert_data_func = lambda q, cat: generated_id
        result = topics_services.create(Topics(title="title",description="test dec", creation_date="2022-10-10",author_id=8,category_id=1),insert_data_func)
        excepted = Topics(id=generated_id,title="title", description="test dec", creation_date="2022-10-10",author_id=8,category_id=1)

        self.assertEqual(excepted, result)
    
    def test_if_topic_is_locked_returnTrue(self):
        update_data_func = lambda q, id: True

        result = topics_services.lock_topic(1, update_data_func)
        expected = True

        self.assertEqual(expected, result)

    def test_if_topic_is_unlocked_returnFalse(self):
        update_data_func = lambda q, id: False

        result = topics_services.unlock_topic(1, update_data_func)
        expected = False

        self.assertEqual(expected, result)