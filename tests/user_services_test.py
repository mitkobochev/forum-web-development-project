import unittest
from server.data.models import Users
from server.services import user_services


class UsersService_Should(unittest.TestCase):

    def test_find_by_username_works_correctly(self):
        get_data_func = lambda q,u: [(8,'steven2','steven2','member')]
        result = user_services.find_by_username("steven2",get_data_func)
        expected = Users(id=8, username='steven2', password='steven2', user_role='member')

        self.assertEqual(expected,result)

    def test_createUser_works_correctly(self):
        generated_id = 1
        insert_data_func = lambda q,u: generated_id

        result = user_services.create("steven2","steven2", insert_data_func)
        expected = Users(id=1, username='steven2', password='', user_role='member')

        self.assertEqual(expected,result)

    def test_if_user_is_authenticated_works_correctly_whenUserIsEmpty(self):
        get_data_func = lambda q,u: []

        result = user_services.is_authenticated("10;pesho", get_data_func)
        expected = False

        self.assertEqual(expected,result)
    
    def test_if_user_is_authenticated_works_correctly_whenUserIsCorrectlyEnetered(self):
        get_data_func = lambda q,u: [(10,"pesho","pesho","member")]

        result = user_services.is_authenticated("10;pesho", get_data_func)
        expected = True

        self.assertEqual(expected,result)
    
    def test_if_promoteToCategoryMember_returns_True(self):
        update_data_func = lambda u,p: True

        result = user_services.promote_to_category_member("steven2", update_data_func)
        expected = True

        self.assertEqual(expected, result)
    
    def test_if_demote_returns_False(self):
        update_data_func = lambda u,p: False

        result = user_services.demote("steven2", update_data_func)
        expected = False

        self.assertEqual(expected, result)