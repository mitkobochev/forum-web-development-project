import unittest
from unittest.mock import Mock, create_autospec, patch
from server.data.models import Category, Topics
from server.routers import categories as categories_router
from server.common.response import NotFound


mock_category_service = Mock(spec='server.services.category_services')
mock_topic_service = Mock(spec = 'server.services.topics_services')
categories_router.category_services = mock_category_service
categories_router.topics_services = mock_topic_service

def fake_category(id= 1, name='test', is_private=True, is_locked=False):
    mock_category = Mock(spec=Category)
    mock_category.id = id
    mock_category.name = name
    mock_category.is_private = is_private
    mock_category.is_locked = is_locked
    return mock_category

def fake_topic(id =1, title='fake_title',description='fake_description', creation_date ='2022-10-10',author_id = 8, category_id = 1): 
    mock_topic = Mock(spec=Topics)
    mock_topic.id = id
    mock_topic.title = title
    mock_topic.description = description
    mock_topic.creation_date = creation_date
    mock_topic.author_id = author_id
    mock_topic.category_id = category_id
    return mock_topic

class CategoryRouterShould(unittest.TestCase):

    def setUp(self) -> None:
        mock_category_service.reset_mock()
        mock_topic_service.reset_mock()
    
    def test_getCategoryById_returns_NotFound_when_noCategory(self):
        mock_category_service.get_by_id = lambda id: None

        result = type(categories_router.get_category_by_id(1,"10;pesho"))
        expected = NotFound

        self.assertEqual(expected,result)
    
    def test_getCategoryById_returns_CategoryResponse_when_categoryIsPresent(self):
        category = fake_category()
        topics = fake_topic()
        mock_category_service.get_by_id = lambda id: category
        mock_topic_service.get_topic_by_category = lambda cat_id: [topics] 

        result = categories_router.get_category_by_id(category.id,"10;pesho")
        expected = categories_router.CategoryResponseModel(category=category,topics=[topics])
        
        self.assertEqual(expected, result)
        self.assertEqual(expected.category, result.category)
        self.assertEqual(expected.topics, result.topics)
    
    def test_getCategories_returns_emptyList_when_noCategories(self):
        mock_category_service.all = lambda: []

        self.assertEqual([],categories_router.get_categories('10;pesho'))

    def test_getCategories_returns_listOfResponseModels_when_categoriesArePresent(self):
        category0 = fake_category(id=0)
        category1 = fake_category(id=1)
        topics1 = fake_topic(category_id=1)
        topic_pool =[[],[topics1]]

        mock_category_service.all = lambda: [category0,category1]
        mock_topic_service.get_topic_by_category = lambda cat_id: topic_pool[cat_id]

        result = categories_router.get_categories('10;pesho')
        expected = [
            categories_router.CategoryResponseModel(category=category0,topics=[]),
            categories_router.CategoryResponseModel(category=category1,topics=[topics1])
        ]

        self.assertEqual(expected, result)
    
    def test_createCategory_returns_ResponseModelWithEmptyTopicList_when_categoryIsValid(self):
        category = fake_category()
        mock_category_service.create = lambda cat: category

        result  = categories_router.create_category(category,"10;pesho")
        expected = categories_router.CategoryResponseModel(category=category,topics=[])

        self.assertEqual(expected, result)
    
    def test_makePrivateCategory_returns_True_when_categoryIsValid(self):
        mock_category_service.make_private = lambda is_private: True

        result = categories_router.make_private(1, '10;pesho')
        expected = "Category is now private!"

        self.assertEqual(expected, result)
    
    def test_makePublicCategory_returns_True_when_categoryIsValid(self):
        category = fake_category()
        mock_category_service.make_public = lambda is_private: category.is_private

        result = categories_router.make_public(1, '10;pesho')
        expected = "Category is now public!"

        self.assertEqual(expected, result)

    def test_lockCategory_returns_True_when_categoryIsValid(self):
        mock_category_service.locked = lambda locked: locked

        result = categories_router.lock_category(1, '10;pesho')
        expected = "Category has been locked successfully!"

        self.assertEqual(expected, result)
    
    def test_unlockedCategory_returns_True_when_categoryIsValid(self):
        mock_category_service.unlock = lambda unlocked: unlocked

        result = categories_router.unlock_category(1, '10;pesho')
        expected = "Category has been unlocked successfully!"

        self.assertEqual(expected, result)