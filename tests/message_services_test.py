import unittest
from server.data.models import Messages
from server.services import message_services

class MessageService_Should(unittest.TestCase):

    def test_all_creatMessage_when_dataIsPresent(self):
        get_data_func = lambda q: [
            (1,"Hello_Fake",8,10), (2,"Welcome_Fake",10,8)]

        result = list(message_services.all(None, get_data_func))

        self.assertEqual(2, len(result))
    
    def test_if_message_is_returnedById(self):
        get_data_func = lambda q,id: [
            (1,"Hello_Fake",8,10)]
        result = message_services.get_by_id(1,get_data_func)
        expected = Messages(id=1,text="Hello_Fake",sender_id=8,reciever_id=10)

        self.assertEqual(expected,result)

    def test_if_message_is_created(self):
        generated_id = 1
        insert_data_func = lambda q, rep: generated_id
        result = message_services.create_message(Messages(text="Hello_Fake",sender_id=8,reciever_id=10),
        insert_data_func)
        expected = result

        self.assertEqual(expected, result)