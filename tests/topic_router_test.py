import unittest
from unittest.mock import Mock, create_autospec, patch
from server.data.models import Replies, Topics
from server.routers import topics as topic_router
from server.common.response import NotFound

mock_topic_service = Mock(spec = 'server.services.topics_services')
mock_reply_service = Mock(spec='server.services.replies_service')
topic_router.topics_services = mock_topic_service
topic_router.replies_service = mock_reply_service

def fake_topic(id =1, title='fake_title',description='fake_description', creation_date ='2022-10-10',author_id = 8, category_id = 1): 
    mock_topic = Mock(spec=Topics)
    mock_topic.id = id
    mock_topic.title = title
    mock_topic.description = description
    mock_topic.creation_date = creation_date
    mock_topic.author_id = author_id
    mock_topic.category_id = category_id
    return mock_topic

def fake_reply(id=1, text="fake_reply",creation_date="2022-10-10",votes=5,topic_id=1,user_id=8,is_best_reply=True):
    mock_reply = Mock(spec=Replies)
    mock_reply.id = id
    mock_reply.text = text
    mock_reply.creation_date = creation_date
    mock_reply.votes = votes
    mock_reply.topic_id = topic_id
    mock_reply.user_id = user_id
    mock_reply.is_best_reply = is_best_reply
    return mock_reply

class TopicRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_topic_service.reset_mock()
        mock_reply_service.reset_mock()

    def test_getTopicById_returns_NotFound_when_noTopic(self):
        mock_topic_service.get_by_id = lambda id: None

        result = type(topic_router.get_topic_by_id(1,"10;pesho"))
        expected = NotFound

        self.assertEqual(expected,result)
    
    def test_getTopicById_returns_TopicResponse_when_topicIsPresent(self):
        topic = fake_topic()
        mock_topic_service.get_by_id = lambda id: topic
        mock_topic_service.return_all_topics_by_id = lambda id: topic

        result = topic_router.get_topic_by_id(1, "10;pesho")
        self.assertEqual(topic, result)
    
    def test_getTopics_returns_emptyList_when_noTopics(self):
            mock_topic_service.all = lambda: []

            self.assertEqual([],topic_router.get_topics("10;pesho"))

    def test_createTopic_returns_ResponseModelWithEmptyReplyList_when_TopicIsValid(self):
        topic0 = fake_topic(id=0)
        topic1 = fake_topic(id=1)
        replies1 = fake_reply(topic_id=1)
        reply_pool =[[],[replies1]]

        mock_topic_service.all = lambda: [topic0,topic1]
        mock_reply_service.get_replies_by_topic = lambda top_id: reply_pool[top_id]

        result  = topic_router.get_topics("10;pesho")
        expected =[
            topic_router.TopicResponseModel(topic=topic0,replies=[]),
            topic_router.TopicResponseModel(topic=topic1,replies=[replies1])
        ]
    
        self.assertEqual(expected, result)
    
    def test_createTopic_returns_ResponseModelWithEmptyReplyList_when_categoryIsValid(self):
        topic = fake_topic()
        mock_topic_service.create = lambda top: topic

        result = topic_router.create_topic(topic,"10;pesho")
        self.assertEqual(topic, result)
    
    def test_lockTopic_returns_True(self):
        mock_topic_service.lock_topic = lambda lock: True

        result = topic_router.lock_topic(1,"10;pesho")
        expected = 'Topic has been locked successfully!'
        
        self.assertEqual(expected, result)

    def test_unlockTopic_returns_True(self):
        mock_topic_service.unlock_topic = lambda lock: False

        result = topic_router.unlock_topic(1,"10;pesho")
        expected = 'Topic has been unlocked successfully!'
        
        self.assertEqual(expected, result)