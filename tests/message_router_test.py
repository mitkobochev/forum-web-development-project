import unittest
from unittest.mock import Mock
from server.data.models import Messages
from server.routers import messages as message_router
from server.common.response import NotFound

mock_message_service = Mock(spec='server.services.message_services')
message_router.message_services = mock_message_service


def fake_message(id=3, text="Hello!", sender_id=8, reciever_id=10):
    mock_message = Mock(spec=Messages)
    mock_message.id = id
    mock_message.text = text
    mock_message.sender_id = sender_id
    mock_message.reciever_id = reciever_id

    return mock_message


class MessageRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_message_service.reset_mock()

    def test_getMessage_returns_emptyList_when_noMessages(self):
        fake = fake_message()
        mock_message_service.all = lambda id: []
        result = message_router.get_messages(None,"10;pesho")

        self.assertEqual([], result)

    def test_getMessage_returns_Message_when_dataIsPresent(self):
        message = fake_message()
        mock_message_service.all = lambda messages: message

        result = message_router.get_messages(None,"10;pesho")
     
        self.assertEqual(message, result)

    def test_getMessageById_returns_NotFound_when_noMessage(self):
        mock_message_service.get_message_by_user_id = lambda id: None

        result = type(message_router.view_all_messages_by_user(10,"10;pesho"))
        expected = NotFound

        self.assertEqual(expected,result)


    def test_getMessageById_returns_Message_when_data_is_present(self):
        message = fake_message()
        mock_message_service.get_message_by_user_id = lambda id: message

        result = message_router.view_all_messages_by_user(message.sender_id,"8;steven2")

        self.assertEqual(message,result)

    def test_getMessageById_returns_Conversation_when_data_is_present(self):
        message = fake_message()
        mock_message_service.view_conversation = lambda id,m: message

        result = message_router.view_conversation_messages_by_user(message.sender_id,message.reciever_id,"8;steven2")

        self.assertEqual(message,result)
    
    def test_createMessage_returns_correctly(self):
        message = fake_message()
        mock_message_service.create_message = lambda m: message

        result = message_router.create_message(message, "8;steven2")

        self.assertEqual(message, result)