import unittest
from unittest.mock import Mock
from server.data.models import Users, LoginData
from server.routers import users as user_router
from server.common.response import BadRequest

mock_user_service = Mock(spec = 'server.services.user_services')
user_router.topics_services = mock_user_service

def fake_user(id=1,username="steven",password="steven",user_role="member"):
    mock_user = Mock(spec=Users)
    mock_user.id = id
    mock_user.name = username
    mock_user.password = password
    mock_user.user_role = user_role

    return mock_user

def fake_login(username="steven2", password="steven2"):
    mock_login = Mock(spec=LoginData)
    mock_login.username = username
    mock_login.password = password
    return mock_login

def fake_register(username="steven21", password="steven20"):
    mock_login = Mock(spec=LoginData)
    mock_login.username = username
    mock_login.password = password
    return mock_login

class UserRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        mock_user_service.reset_mock()
    
    def test_if_login_works_correctly(self):
        user = fake_login()

        result  = user_router.login(user)
        expected = ("Welcome back! Here's your login infornamtion.", {'token': '8;steven2'})

        self.assertEqual(expected, result)
    
    def test_if_register_returns_correctly_when_BadRequest(self):
        user = fake_register()
        mock_user_service.create = lambda q: user

        result = type(user_router.register(user))
        expected = BadRequest

        self.assertEqual(expected,result)
    
    def test_if_userPromote_correctly(self):
        mock_user_service.promote_to_category_member = lambda q: True

        result = user_router.promote_user("steven2","10;pesho")
        expected = 'User has been promoted to category_member!'

        self.assertEqual(expected, result)

    def test_if_userDemote_correctly(self):
        mock_user_service.demote = lambda q: False

        result = user_router.demote_user("steven2","10;pesho")
        expected = 'User has been demoted to member!'

        self.assertEqual(expected, result)