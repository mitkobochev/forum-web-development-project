import unittest
from unittest.mock import Mock
from server.data.models import Replies, UserVoteModel
from server.routers import replies as reply_router
from server.common.response import Unauthorized

mock_reply_service = Mock(spec='server.services.replies_service')
reply_router.replies_service = mock_reply_service

def fake_reply(id=1, text="fake_reply",creation_date="2022-10-10",votes=5,topic_id=1,user_id=8,is_best_reply=True):
    mock_reply = Mock(spec=Replies)
    mock_reply.id = id
    mock_reply.text = text
    mock_reply.creation_date = creation_date
    mock_reply.votes = votes
    mock_reply.topic_id = topic_id
    mock_reply.user_id = user_id
    mock_reply.is_best_reply = is_best_reply
    return mock_reply

def fake_vote_model(vote_id=1, user_id=10,reply_id=1,vote=1):
    mock_vote = Mock(spec=UserVoteModel)
    mock_vote.vote_id = vote_id
    mock_vote.user_id = user_id
    mock_vote.reply_id = reply_id
    mock_vote.vote = vote


class ReplyRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        mock_reply_service.reset_mock()
    
    def test_getReply_returns_emptyList_when_noReplies(self):
        mock_reply_service.all = lambda: []

        self.assertEqual([],reply_router.get_replies("10;pesho"))
    
    def test_getReplyByID_returns_Reply_when_ReplyIsPresent(self):
        result = fake_reply()
        mock_reply_service.get_by_id = lambda id: result

        result = reply_router.get_reply_by_id(1,"10;pesho")

        self.assertEqual(result, result)
    

    def test_getReplyById_returns_Unauthorized_when_noReplies(self):
        mock_reply_service.get_by_id = lambda id: None

        result = type(reply_router.get_reply_by_id(1,"10;pesho"))
        expected = Unauthorized

        self.assertEqual(expected,result)
    
    def test_createReply_returns_Reply_when_ReplyIsValid(self):
        reply = fake_reply()
        mock_reply_service.get_locked_topics = lambda q: False
        mock_reply_service.get_private_categories = lambda q: False
        mock_reply_service.create = lambda q: reply

        result = reply_router.create_reply(reply,"10;pesho")

        self.assertEqual(reply,result)