from fastapi import FastAPI
from server.routers.categories import categories_router
from server.routers.topics import topic_router
from server.routers.replies import replies_router
from server.routers.users import users_router
from server.routers.messages import messages_router


app = FastAPI()
app.include_router(categories_router)
app.include_router(topic_router)
app.include_router(replies_router)
app.include_router(users_router)
app.include_router(messages_router)