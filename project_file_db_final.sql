-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema project_file_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema project_file_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `project_file_db` DEFAULT CHARACTER SET latin1 ;
USE `project_file_db` ;

-- -----------------------------------------------------
-- Table `project_file_db`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`categories` (
  `category_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `is_private` TINYINT(4) NULL DEFAULT NULL,
  `is_locked` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `project_file_db`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `user_role` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `project_file_db`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`messages` (
  `message_id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(200) NOT NULL,
  `sender_id` INT(11) NOT NULL,
  `reciever_id` INT(11) NOT NULL,
  PRIMARY KEY (`message_id`, `sender_id`, `reciever_id`),
  INDEX `fk_messages_users1_idx` (`sender_id` ASC) VISIBLE,
  INDEX `fk_messages_users2_idx` (`reciever_id` ASC) VISIBLE,
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`sender_id`)
    REFERENCES `project_file_db`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_users2`
    FOREIGN KEY (`reciever_id`)
    REFERENCES `project_file_db`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `project_file_db`.`replies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`replies` (
  `reply_id` INT(11) NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(200) NOT NULL,
  `creation_date` DATETIME NOT NULL,
  `topic_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `votes` INT(11) NOT NULL,
  `is_best_reply` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`reply_id`),
  INDEX `fk_replies_topics1_idx` (`topic_id` ASC) VISIBLE,
  INDEX `fk_replies_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_replies_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `project_file_db`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 31
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `project_file_db`.`topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`topics` (
  `topic_id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(200) NOT NULL,
  `creation_date` DATETIME NOT NULL,
  `author_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `is_locked` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`topic_id`),
  INDEX `fk_topics_Users_idx` (`author_id` ASC) VISIBLE,
  INDEX `fk_topics_categories1_idx1` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_topics_Users`
    FOREIGN KEY (`author_id`)
    REFERENCES `project_file_db`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_topics_categories1`
    FOREIGN KEY (`category_id`)
    REFERENCES `project_file_db`.`categories` (`category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 146
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `project_file_db`.`users_replies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`users_replies` (
  `user_id` INT(11) NOT NULL,
  `reply_id` INT(11) NOT NULL,
  PRIMARY KEY (`user_id`, `reply_id`),
  INDEX `fk_users_has_replies_replies1_idx` (`reply_id` ASC) VISIBLE,
  INDEX `fk_users_has_replies_users1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_replies_replies1`
    FOREIGN KEY (`reply_id`)
    REFERENCES `project_file_db`.`replies` (`reply_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_replies_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `project_file_db`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `project_file_db`.`votes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project_file_db`.`votes` (
  `vote_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `reply_id` INT(11) NOT NULL,
  `vote` INT(11) NOT NULL,
  PRIMARY KEY (`vote_id`),
  INDEX `fk_votes_users1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_votes_replies1_idx` (`reply_id` ASC) VISIBLE,
  CONSTRAINT `fk_votes_replies1`
    FOREIGN KEY (`reply_id`)
    REFERENCES `project_file_db`.`replies` (`reply_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_votes_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `project_file_db`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 63
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
