# Web Dev Team Work Project

Members - Mitko Bochev, Niki Vedzhov

## GitLab Link to main files

Main files link: https://gitlab.com/Niksomus/web-dev-team-work-project

Access to project: 
- Niki Vedzhov
- Mitko Bochev
- Grigor Mironov
- Edward Georgiev
- Vladimir Venkov

## Tools used for Web Teamwork Project

Visual Studio Code - for Python code creation
Framework - Fast API
Postman - for building and using Fast API
MariaDB - for the creation of a working database
Microsoft Workbench - for the illustration and editing of used database

----------------------------------------------------------------------------------------------------------------------------------------------

## Description
Design and implement a Forum System and provide RESTful API that can be consumed by different clients. High-level description:

· Users can read and create topics and message other users
· Administrators manage users, topics and categories

## Project status
Completed all MUST and SHOULD requirements on project.

## Project functionalities
All project functionalities are described as comments on their respective pages and rows. Below you can view all the implemented functions of the project.

# Token Endpoint

- Accepts user login data
- Responds with authentication token that can be used to access other endpoints.

# Register User

- Accepts user registration data
- Considereded making at least one user property unique for login purposes.

# Create Topic

- Requires authentication token
- Topic data must contain at least a title and a Category

# Create Reply

- Requires authentication token
- Reply data should contain at least text and is associated with a specific Topic

# View Topics

- Responds with a list of Topic resources.
- Considereded adding search, sort and pagination query params

# View Topic

- Responds with a single Topic resource and a list of Reply resources

# View Category

- Responds with a list of all Topics that belong to that Category
- Considered adding search, sort and pagination query params

# View Categories

- Responds with a list of all Categories

# Create Message

- Requires authentication
- Creates a Message, should contain at least text as property
- Messages should be addressed to a specific user

# View Conversation

- Requires authentication
- Responds with a list of Messages exchanged between the authenticated user and another user

# View Conversations

- Requires authentication
- Responds with a list of all Users with which the authenticated user has exchanged messages

# Upvote/Downvote a Reply

- Requires authentication
- A user should be able to change their downvotes to upvotes and vice versa but a reply can only be upvoted/downvoted once per user

# Choose Best Reply

- Requires authentication
- Topic Author can select one best reply to their Topic


# Create Category

- Requires admin authentication
- Category data should contain at least a name

# Make Category Private / Non-private

- Requires admin authentication
- Changes visibility to a category and all associated topics
- Topics in a private category are only available to category members

# Give User a Category Read Access

- Requires admin authentication
- A user can now view all Topics and Replies in the specific private Category

# Give User a Category Write Access

- Requires admin authentication
- A user can now view all Topics and Replies in the specific private Category and post new Topics and Replies

# Revoke User Access

- Requires admin authentication
- A user loses their read or write access to a category

# View Privileged Users

- Requires admin authentication
- Responds with a list of all users for a specific Private Category along with their Access Level

# Lock Topic

- Requires admin authentication
- A Topic can no longer accept new Replies

# Lock Category

- Requires admin authentication
- A category can no longer accept new Topics