from fastapi import APIRouter, Header
from server.data.models import Messages
from server.common.response import NotFound, Unauthorized
from server.services import message_services
from server.common.auth import get_user_or_raise_401

messages_router = APIRouter(prefix='/messages')

# A GET method to receive all the messages from all users
# This method is applicable only by admins
# URL Example: http://localhost:8000/messages (admin token required)
@messages_router.get('/')
def get_messages(search: str = None,x_token: str = Header()):
    member = get_user_or_raise_401(x_token)
    if not member.is_admin():
        return Unauthorized("Admin restriction")
        
    return message_services.all(search)

# A GET method to receive all the messages from a single users
# This method is applicable by all logged in users
# URL Example: http://localhost:8000/messages/user_id (token required; user_id is an integer)
@messages_router.get('/{user_id}')
def view_all_messages_by_user(user_id: int | str, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    message = message_services.get_message_by_user_id(user_id)

    if not member.id == user_id:
        return f'You cannot look at others messages'
        
    if not message or None:
        return NotFound("You have no permission for other member messages,\nor there is no messages in your inbox")

    return message

# A GET method to receive all the messages between two users
# This method is applicable by all logged in users
# URL Example: http://localhost:8000/messages/conversation/sender_id-receiver_id (token required; sender_id and receiver_id are integers)
@messages_router.get('/conversation/{sender_id}-{reciever_id}')
def view_conversation_messages_by_user(sender_id: int | str,reciever_id: int | str, x_token = Header()):
    member = get_user_or_raise_401(x_token)

    if not member.id == sender_id:
        return Unauthorized("You cannot use your login to view others messages")
    message = message_services.view_conversation(sender_id,reciever_id)

    return message

# A POST method to create a message from one user to another
# This method is applicable by all logged in users
# URL Example: http://localhost:8000/messages (token required)
# Body required: Text, Sender_ID, Receiver_ID
@messages_router.post('/', status_code=201)
def create_message(message: Messages,x_token = Header()):
    sender_id_convert_to_int = int(message.sender_id)
    member = get_user_or_raise_401(x_token)
    if not member.id == sender_id_convert_to_int:
        return Unauthorized("You need to login first to send a message")
        
    return message_services.create_message(message)

# A PUT method to update a message from one user to another
# This method is applicable by all logged in users
# URL Example: http://localhost:8000/messages/edit/id (token required, id is an integer)
# Body required: Text
@messages_router.put('/edit/{id}', status_code=201)
def update_replies(id: int, messages: Messages,x_token = Header()):
    existing_message = message_services.get_by_id(id)
    member = get_user_or_raise_401(x_token)
    if member.id != int(existing_message.sender_id):
        return Unauthorized("You need to login first to send a message")
    if existing_message is None:
        return NotFound()
    else:
        message_services.update(existing_message,messages)
        return f"Messaged edited successfully with: {messages.text}"
    
# A DELETE method to delete a specific message
# This method is applicable by all logged in users
# URL Example: http://localhost:8000/messages/delete/id (token required, id is an integer)
@messages_router.delete('/delete/{message_id}')
def remove_message(message_id: int,x_token = Header()):
    member = get_user_or_raise_401(x_token)
    message = message_services.get_by_id(message_id)
    
    if not member.id == int(message.sender_id):
        return Unauthorized("You have no permission to delete others messages!")
    message_services.delete_message(message)

    return f'Message with ID:{message.id} successfully deleted!'

