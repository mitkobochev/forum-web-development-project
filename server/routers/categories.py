from fastapi import APIRouter, Header
from pydantic import BaseModel
from server.data.models import Category,Topics
from server.common.response import NotFound, Unauthorized
from server.services import category_services
from server.services import topics_services
from server.common.auth import get_user_or_raise_401

categories_router = APIRouter(prefix='/categories')

class CategoryResponseModel(BaseModel): # A class used to better illustrate the shown category and it's list of created topics.
    category: Category
    topics: list[Topics]

# A GET method used to retrieve the information for all Categories and their associated Topics.
# Only logged in members can view this information
# Only admins and category members can view private Categories
# URL Example: http://localhost:8000/categories (token required)
@categories_router.get('/')
def get_categories(x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin() or member.is_category_member():
        return [
            CategoryResponseModel(
                category=category,
                topics=topics_services.get_topic_by_category(category.id))
                for category in category_services.all()]
    else:
        return [
            CategoryResponseModel(
                category=category,
                topics=topics_services.get_topic_by_category(category.id))
                for category in category_services.all_public()]

# A GET method used to retrieve the information for a specific Category and all Topics associated with it's related Topics
# Only logged in members can view this information
# Only admins and category members can view private Categories
# URL Example: http://localhost:8000/categories/id (token required)
@categories_router.get('/{id}')
def get_category_by_id(id: int, x_token: str = Header()):
    member = get_user_or_raise_401(x_token)
    category = category_services.get_by_id(id)
    if not member.is_admin() and not member.is_category_member() and category.is_private == True:
        return Unauthorized("Only admins and category members can view this private category!")
    if category is None:
        return NotFound()
    else:   
        return CategoryResponseModel(category = category, topics=topics_services.get_topic_by_category(category.id))

# A POST method used to create information for a newly created Category
# Only logged in admins can create this information
# Only admins can create private Categories
# URL Example: http://localhost:8000/categories (admin token required)
# Body required: Name, 
@categories_router.post('/')
def create_category(category: Category,x_token= Header()):
    member = get_user_or_raise_401(x_token)
    if not member.is_admin():
        return Unauthorized("Only an admin can create new categories!")
    create_category = category_services.create(category)

    return CategoryResponseModel(category=create_category, topics=[])

# A PUT method used to update a specific Category to a private status
# Only logged in admins can update this information
# Only admins can update the Category to private
# URL Example: http://localhost:8000/categories/id/make-private (admin token required)
@categories_router.put('/{category_id}/make-private', status_code= 201)
def make_private(category_id: int, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        make_it_private = category_services.make_private(category_id)
    else:
        return Unauthorized("Only an admin can make a category private!")
    
    return f'Category is now private!'

# A PUT method used to update a specific Category to a public status
# Only logged in admins can update this information
# Only admins can update the Category to private
# URL Example: http://localhost:8000/categories/id/make-public (admin token required)
@categories_router.put('/{category_id}/make-public', status_code= 201)
def make_public(category_id: int,x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        make_it_public = category_services.make_public(category_id)
    else:
        return Unauthorized("Only an admin can make a category public!")
    return f'Category is now public!'

# A PUT method used to update a specific Category to a locked status
# Only logged admins can lock this information
# Only admins can update the Category to locked
# URL Example: http://localhost:8000/categories/id/lock (admin token required)
@categories_router.put('/{category_id}/lock', status_code= 201)
def lock_category(category_id: int, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        lock_category = category_services.locked(category_id)
    else:
        return Unauthorized("Only an admin can lock a categories!")
    
    return f"Category has been locked successfully!"

# A PUT method used to update a specific Category to a unlocked status
# Only logged admins can unlock this information
# Only admins can update the Category to unlocked
# URL Example: http://localhost:8000/categories/id/unlock (admin token required)
@categories_router.put('/{category_id}/unlock', status_code= 201)
def unlock_category(category_id: int, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        unlock_category = category_services.unlock(category_id)
    else:
        return Unauthorized("Only an admin can unlock a categories!")
    
    return f'Category has been unlocked successfully!'