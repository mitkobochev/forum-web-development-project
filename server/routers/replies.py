from fastapi import APIRouter, Header
from server.data.models import Replies, UserVoteModel
from server.common.response import NotFound, BadRequest, Unauthorized
from server.services import replies_service
from server.services import topics_services, category_services
from server.common.auth import get_user_or_raise_401

replies_router = APIRouter(prefix='/replies')

# A GET method to receive all the replies from all users
# Only logged in members can view this information
# Only admins and category members can view replies in private Topics
# URL Example: http://localhost:8000/topics (token required)
@replies_router.get('/')
def get_replies(
    x_token: str = Header(),
    sort: str | None = None,
    sort_by: str | None = None,
    search: str | None = None
):
    member = get_user_or_raise_401(x_token)
    if sort and (sort == 'asc' or sort == 'desc'):
        return replies_service.sort(reverse=sort == 'desc', attribute=sort_by)
    if member.is_admin() or member.is_category_member():
        return replies_service.all()
    else:
        return f'Only admins and category members have access on this section'

# A GET method to receive a specific reply
# Only logged in members can view this information
# Only admins and category members can view replies in private Topics
# URL Example: http://localhost:8000/topics/id (token required; id is an integer)    
@replies_router.get('/{id}')
def get_reply_by_id(id: int,x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin() or member.is_category_member():
        return replies_service.get_by_id(id) or Unauthorized("This reply may not exist!")
    else:
        return replies_service.get_by_id_restricted(id) or Unauthorized("You may have no permission to access this reply!")

# A POST method to create a specific reply
# Only logged in members can create this information
# No replies can be posted if the Topic is locked by an admin
# Only admins and category members can create replies in private Topics
# URL Example: http://localhost:8000/topics (token required)     
# Body required: text, creation_date, topic_id, user_id, votes
@replies_router.post('/', status_code=201)
def create_reply(reply: Replies,x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if not topics_services.exist(reply.topic_id):
        return BadRequest('Reply with this topic ID does not exist')

    get_topic = topics_services.get_by_id(reply.topic_id)
    get_category = category_services.get_by_id(get_topic.category_id)

    if not replies_service.get_locked_topics(get_topic.id):
        if replies_service.get_private_categories(get_topic.category_id):
            return replies_service.create(reply)
        elif member.is_admin() or member.is_category_member():
            return replies_service.create(reply)
        else:
            return Unauthorized("You cannot reply in this topic!")
    else:
        return Unauthorized("You cannot post this reply because the Topic is locked!")

# A PUT method to update a specific reply
# Only logged in members can update this information
# No replies can be updated if the Topic is locked by an admin
# Only admins and category members can update replies in private Topics
# URL Example: http://localhost:8000/topics/id (token required; id must be an integer) 
# Body required: text, creation_date
@replies_router.put('/{id}', status_code=201)
def update_replies(id: int, reply: Replies,x_token = Header()):
    member = get_user_or_raise_401(x_token)
    existing_reply = replies_service.get_by_id(id)

    get_topic = topics_services.get_by_id(reply.topic_id)
    get_cateogry = category_services.get_by_id(get_topic.category_id)
    if not replies_service.get_locked_topics(reply.topic_id):
        if existing_reply is None:
            return NotFound()
        if replies_service.get_private_categories(get_cateogry.id):
            return replies_service.update(existing_reply, reply)
        elif member.is_admin() or member.is_category_member():
            return replies_service.update(existing_reply, reply)
        else:
            return Unauthorized("You cannot edit this reply in this topic!")
    else:
        return Unauthorized("This topic is locked by an admin and no longer accepts new replies!")
    
# A PUT method to like a specific reply
# Only logged in members can like replies
# All logged in members can like replies
# If a user "likes" a reply a second time the like is nulled
# URL Example: http://localhost:8000/topics/reply_id/like (token required; reply_id must be an integer)     
# Body required: user_id, reply_id, vote
@replies_router.put('/{reply_id}/like', status_code=201)
def like(reply_id: int, like: UserVoteModel,x_token: str = Header()):
    member = get_user_or_raise_401(x_token)
    replies = replies_service.get_vote_id(member.id, reply_id)
    
    if replies is None:
       return replies_service.insert_vote_if_None(like)
    elif replies is not None:
            if replies.user_id == member.id:
                return replies_service.update_upvote(replies, like)

            else:
                return (f'You cannot vote from others accounts!')
            
# A PUT method to select a best reply
# Only logged in members can like replies
# All logged in members can like replies
# Only one best reply can exist within a Topic
# URL Example: http://localhost:8000/topics/reply_id/best-reply (token required; reply_id must be an integer) 
@replies_router.put('/{reply_id}/best-reply', status_code= 201)
def best_reply(reply_id: int, x_token: str = Header()):
    result = replies_service.is_best_reply(reply_id)
    
    return f'You have chosen reply with ID:{reply_id} to be this best reply!'