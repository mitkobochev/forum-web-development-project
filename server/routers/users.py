from fastapi import APIRouter, Header
from server.common.response import BadRequest
from server.common.auth import get_user_or_raise_401
from server.data.models import LoginData, Replies
from server.services import user_services
from server.services import replies_service
from server.common.response import Unauthorized


users_router = APIRouter(prefix='/users')

# A POST method used to login a member of the forum.
# A token is created for a user once he/she is logged in.
# URL Example: http://localhost:8000/login
@users_router.post('/login')
def login(data: LoginData):
    user = user_services.try_login(data.username, data.password)

    if user:
        token = user_services.create_token(user)
        return "Welcome back! Here's your login infornamtion.",{'token': token}
    else:
        return BadRequest('Invalid login data')

# A POST method used to register a new member in the forum.
# URL Example: http://localhost:8000/register
# Body required: username, password
@users_router.post('/register')
def register(data: LoginData):
    user = user_services.create(data.username, data.password)

    return user or BadRequest(f'Username {data.username} is taken.')

# A GET method used to retrieve the information of the logged in user.
# A member has to be logged in to retrieve the information.
# URL Example: http://localhost:8000/info
@users_router.get('/info')
def user_info(x_token: str = Header()):
    return get_user_or_raise_401(x_token)

# A GET method used to retrieve all the replies of the logged in user.
# A member has to be logged in to retrieve the information.
# URL Example: http://localhost:8000/replies
@users_router.get('/replies/', response_model=list[Replies])
def user_info(x_token: str = Header()):
    user = get_user_or_raise_401(x_token)

    return replies_service.get_user_replies(user)

# A PUT method used to promote an existing user.
# A member has to be an admin to promote others.
# URL Example: http://localhost:8000/username/promote (username has to be an existing user)
@users_router.put('/{username}/promote', status_code= 201)
def promote_user(username: str, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        promote_member = user_services.promote_to_category_member(username)
    else:
        return Unauthorized("Only an admin can promote members!")
    
    return f'User has been promoted to category member!'

# A PUT method used to demote an existing user.
# A member has to be an admin to demote others.
# URL Example: http://localhost:8000/username/demote (username has to be an existing user)
@users_router.put('/{username}/demote', status_code= 201)
def demote_user(username: str, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        promote_member = user_services.demote(username)
    else:
        return Unauthorized("Only an admin can demote members!")
    
    return f'User has been demoted to member!'

# A GET method used by an admin to view the information of all existing category members (excluding their passwords).
# A member has to be an admin to view this information.
# URL Example: http://localhost:8000/username/access
@users_router.get('/access/', status_code=201)
def access_users(x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        return user_services.access()
