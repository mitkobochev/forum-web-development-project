from fastapi import APIRouter, Header
from server.services import topics_services, category_services, replies_service
from server.data.models import Category, Topics, Replies
from pydantic import BaseModel
from server.common.response import NotFound, BadRequest, Unauthorized
from server.common.auth import get_user_or_raise_401

topic_router = APIRouter(prefix='/topics')


class TopicResponseModel(BaseModel): ## A class used to better illustrate the shown topic and it's list of created replies.
    topic: Topics
    replies: list[Replies]

# A GET method used to retrieve the information for all Topics and their associated replies.
# Only logged in members can view this information
# Only admins and category members can view private Topics
# URL Example: http://localhost:8000/topics (token required)
@topic_router.get('/')
def get_topics(
    x_token: str = Header(),
    sort: str | None = None,
    sort_by: str | None = None,
    search: str | None = None
):
    member = get_user_or_raise_401(x_token)
    if sort and (sort == 'asc' or sort == 'desc'):
        return topics_services.sort(reverse=sort == 'desc', attribute=sort_by)
    if member.is_admin() or member.is_category_member():
        return [
            TopicResponseModel(
                topic=topic,
                replies=replies_service.get_replies_by_topic(topic.id))
            for topic in topics_services.all()]
    else:
        return [
            TopicResponseModel(
                topic=topic,
                replies=replies_service.get_replies_by_topic(topic.id))
            for topic in topics_services.public(search)]

# A GET method used to retrieve the information for a specific Topics and it's associated replies.
# Only logged in members can view this information
# Only admins and category members can view private Topics
# URL Example: http://localhost:8000/topics/id (token required; id must be an integer)
@topic_router.get('/{id}')
def get_topic_by_id(id: int, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    topic = topics_services.get_by_id(id)
    if topic is None:
        return NotFound()
    elif member.is_admin() or member.is_category_member():
        return topics_services.return_all_topics_by_id(id)
    else:
        return topics_services.return_restricted_topics_by_id(id)

# A POST method used to create the information for a Topic.
# Only logged in members can create this information
# Only admins and category members can create private Topics
# URL Example: http://localhost:8000/topics (token required)
# Body required: title, description, creation_date, author_id, category_id
@topic_router.post('/', status_code=201)
def create_topic(topic: Topics, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    if not category_services.get_by_id_if_locked(topic.category_id) or member.is_admin():
        if not category_services.exist(topic.category_id):
            return BadRequest('Topic with ID:{topic.category_id} does not exist')

        if category_services.get_by_id_if_private(topic.category_id):
            return topics_services.create(topic)
        elif member.is_admin() or member.is_category_member():
            return topics_services.create(topic)
        else:
            return Unauthorized("You have no permission to make a topic in this category!")
    else:
        return Unauthorized("This category is locked by an admin and no longer accepts new topics!")

# A PUT method used to update the information for a Topic.
# Only logged in members can update this information
# Only admins and category members can update private Topics
# URL Example: http://localhost:8000/topics/id (token required; id must an integer)
# Body required: title, description
@topic_router.put('/{id}', status_code=201)
def update_topic(id: int, topic: Topics, x_token=Header()):
    member = get_user_or_raise_401(x_token)
    existing_product = topics_services.get_by_id(id)
    if existing_product is None:
        return NotFound()
    if category_services.get_by_id_if_private(topic.category_id):
        return topics_services.update(existing_product, topic)
    elif member.is_admin() or member.is_category_member():
        return topics_services.update(existing_product, topic)
    else:
        return Unauthorized("Only an admin or category member can only edit topics!")

# A PUT method used to update the information for a Topic to become locked.
# Only admins can lock this information
# URL Example: http://localhost:8000/topics/id/lock (token required; id must an integer)
@topic_router.put('/{topic_id}/lock', status_code= 201)
def lock_topic(topic_id: int, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        lock_topic = topics_services.lock_topic(topic_id)
    else:
        return Unauthorized("Only admin can lock topics!")
    
    return f"Topic has been locked successfully!"

# A PUT method used to update the information for a Topic to become unlocked.
# Only admins can unlocked this information
# URL Example: http://localhost:8000/topics/id/unlock (token required; id must an integer)
@topic_router.put('/{topic_id}/unlock', status_code= 201)
def unlock_topic(topic_id: int, x_token = Header()):
    member = get_user_or_raise_401(x_token)
    if member.is_admin():
        lock_topic = topics_services.unlock_topic(topic_id)
    else:
        return Unauthorized("Only an admin can unlock topics!")
    
    return f"Topic has been unlocked successfully!"