from fastapi import HTTPException
from server.data.models import Users
from server.services.user_services import is_authenticated, from_token

# A function to see if the user is authenticated(logged in with a token)
def get_user_or_raise_401(token: str) -> Users:
    if not is_authenticated(token):
        raise HTTPException(status_code=401)

    return from_token(token)