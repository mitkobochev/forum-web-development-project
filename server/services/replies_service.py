from server.common.response import NotFound
from server.data.models import Replies, Users, UserVoteModel
from server.data import database

#A function to retrieve the entirety of replies from the database
def all(search: str = None, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    if search is None:
        data = get_data_func(
            '''SELECT reply_id, text, creation_date, votes, topic_id,user_id,is_best_reply from replies order by reply_id''')
    else:
        data = get_data_func(
            '''SELECT reply_id, text, creation_date, (SELECT sum(vote) from votes), topic_id, user_id, is_best_reply from replies
               WHERE text LIKE ?''', (f'%{search}%',))
    return (Replies.from_query_result(*row) for row in data)

#A function to retrieve the entirety of non-private replies from the database, based on publicity of the category
def get_private_categories(id: int, get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    
    data = get_data_func('''select reply_id,text, r.creation_date, r.topic_id, user_id, votes, is_best_reply from replies as r 
        join topics as t on t.topic_id = r.topic_id right 
        join categories
        as c on c.category_id = t.category_id where c.category_id = ? and is_private = 0''',(id,))
    
    return next((Replies.from_query_result(*row) for row in data), None)

#A function to retrieve the entirety of replies from the database
def get_by_id(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    
    data = get_data_func(
        '''SELECT reply_id, text, r.creation_date,
        (SELECT sum(vote) from votes where reply_id = ?),r.topic_id,user_id,is_best_reply from replies as r
        		join topics as t on t.topic_id = r.topic_id
                right join categories as c on c.category_id = t.category_id 

            WHERE r.reply_id = ?''', (id, id))
    return next((Replies.from_query_result(*row) for row in data), None)

#A function to retrieve the a single non-private reply from the database, based on category publicity
def get_by_id_restricted(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    
    data = get_data_func(
        '''SELECT reply_id, text, r.creation_date,
        (SELECT sum(vote) from votes where reply_id = ?),r.topic_id,user_id,is_best_reply from replies as r
        		join topics as t on t.topic_id = r.topic_id
                right join categories as c on c.category_id = t.category_id 

            WHERE r.reply_id = ? and c.is_private = 0''', (id, id))
    return next((Replies.from_query_result(*row) for row in data), None)

#A function to create a reply and store it into the database
def create(reply: Replies, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query
    generated_id = insert_data_func(
        'INSERT INTO replies(text, creation_date, votes,topic_id, user_id,is_best_reply) VALUES(?,?,0,?,?,?)',
        (reply.text, reply.creation_date, reply.topic_id, reply.user_id, reply.is_best_reply))

    reply.id = generated_id

    return reply

#A function to update a reply text and creation date and store it into the database
def update(old: Replies, new: Replies, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    merged = Replies(
        id=old.id,
        text=new.text or old.text,
        creation_date=new.creation_date or old.creation_date,
        votes=old.votes,
        topic_id=old.topic_id,
        user_id=old.user_id,
        is_best_reply=old.is_best_reply)
    update_data_func(
        '''UPDATE replies SET
           text = ?, creation_date = ?
           WHERE reply_id = ? 
        ''',
        (merged.text, merged.creation_date, merged.id))
    return merged

#A function to check if a reply exists in the database
def exists(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    return any(get_data_func('''SELECT reply_id, text, creation_date, votes, topic_id, user_id, is_best_reply from replies 
            WHERE reply_id = ?''', (id,)))

#A function to sort the existing replies by id and votes
def sort(topics: list[Replies], *, attribute='reply_id', reverse=False):
    if attribute == 'vote':
        def sort_fn(r: Replies): return r.votes
    else:
        def sort_fn(r: Replies): return r.id

    return sorted(topics, key=sort_fn, reverse=reverse)

#A function to retrieve all replies, based on the user ID who created the reply
def get_user_replies(user: Users, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func('''SELECT reply_id,text,creation_date,votes,topic_id,ur.user_id,is_best_reply 
            FROM replies AS r
                LEFT JOIN
                users AS ur ON r.user_id = ur.user_id
                WHERE ur.user_id = ?''', (user.id,))

    # flattened_data = _flatten_user_replies(data)

    return (Replies.from_query_result(*obj) for obj in data)


#def _flatten_user_replies(data: list[tuple]):
#    flattened = {}
#    for reply_id, text, creation_date, votes, topic_id, user_id, is_best_reply in data:
#        if reply_id not in flattened:
#            flattened[reply_id] = (text, creation_date, votes, user_id, is_best_reply, [])
#
#       if topic_id is not None:
#            flattened[reply_id][-1].append(topic_id)

#    return flattened

#A function to like a reply and store it into the database. The function resets the vote counter if a reply is already liked.
def update_upvote(old: UserVoteModel, new: UserVoteModel, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query
    merged = UserVoteModel(
        user_id=old.user_id,
        reply_id=old.reply_id,
        vote=new.vote)
    if merged.vote <= 0:
        update_data_func(
            ''' UPDATE votes SET
           vote = 1 WHERE user_id = ? and reply_id = ?
        ''',
            (merged.user_id, merged.reply_id))
    elif merged.vote >= 1:
        update_data_func(
            ''' UPDATE votes SET
           vote = 0 WHERE user_id = ? and reply_id = ?
        ''',
            (merged.user_id, merged.reply_id,))
    return merged

#A function to create a vote column and store it into the database if no such exists
def insert_vote_if_None(vote: UserVoteModel, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query
    try:
        generated_id = insert_data_func(
            'INSERT INTO votes(user_id, reply_id, vote) VALUES(?,?,1)',
            (vote.user_id, vote.reply_id))
    except:
        return NotFound("No such reply or user found!")

    vote.vote_id = generated_id

    return vote

#A function to retrieve a vote from the database based on it's user and reply ID
def get_vote_id(user_id: int, reply_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''SELECT vote_id, user_id, reply_id, vote from votes 
            WHERE user_id = ? and reply_id = ?''', (user_id, reply_id))

    return next((UserVoteModel.from_query_result(*row) for row in data), None)

#A function to retrieve a reply from the database based on it's topic ID
def get_replies_by_topic(topic_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''SELECT reply_id, text, creation_date, topic_id, user_id, votes, is_best_reply
               FROM replies 
            WHERE topic_id = ?''', (topic_id,))

    return (Replies.from_query_result(*row) for row in data)

#A function to update a reply from the database to become a best reply, based on topic ID
def is_best_reply(reply_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(reply_id)
    update_data_func('''UPDATE replies SET
            is_best_reply = False where topic_id = ?''', (result.topic_id,))
    if result:
        return update_data_func('''UPDATE replies SET
            is_best_reply = True WHERE reply_id = ? and topic_id = ?''', (reply_id, result.topic_id,))
    else:
        return NotFound("No such reply exists")

#A function to retrieve a reply from the database based on topic ID and if the topic is currently locked
def get_locked_topics(id: int, get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    
    data = get_data_func('''select reply_id, text, r.creation_date, r.topic_id, user_id, votes, is_best_reply from replies as r 
            join topics as t on t.topic_id = r.topic_id 
            where t.topic_id = ? and is_locked = 1''',(id,))
    
    return next((Replies.from_query_result(*row) for row in data), None)