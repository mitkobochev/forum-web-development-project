from server.data import database
from server.data.models import Category
from server.common.response import NotFound

#A function to retrieve the entirety of categories from the database
def all(get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func('select category_id, name, is_private from categories order by category_id')

    return (Category(id=id, name=name,is_private=is_private) for id, name,is_private in data)

#A function to retrieve the entirety of non-private categories from the database
def all_public(get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func('select category_id, name, is_private from categories where is_private = False')

    return (Category(id=id, name=name,is_private=is_private) for id, name,is_private in data)

#A function to retrieve a single category by it's ID from the database
def get_by_id(id: int,get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func('select category_id, name, is_private, is_locked from categories where category_id = ?', (id,))

    return next((Category(id=id, name=name, is_private=is_private, is_locked=is_locked) for id, name, is_private, is_locked in data), None)

#A function to retrieve a single, non-private category by it's ID from the database
def get_by_id_if_private(id: int,get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func('select category_id, name, is_private, is_locked from categories where category_id = ? and is_private = 0', (id,))

    return next((Category(id=id, name=name, is_private=is_private, is_locked=is_locked) for id, name, is_private, is_locked in data), None)

#A function to retrieve a single, locked category by it's ID from the database
def get_by_id_if_locked(id: int,get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func('select category_id, name, is_private, is_locked from categories where category_id = ? and is_locked = 1', (id,))

    return next((Category(id=id, name=name, is_private=is_private, is_locked=is_locked) for id, name, is_private, is_locked in data), None)

#A function to check if a category exists in the database
def exist(id: int,get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    return any(get_data_func('select category_id, name from categories where category_id = ?', (id,)))

#A function to create a single category
def create(category: Category,insert_data_func = None):
    if insert_data_func is None:
        insert_data_func = database.insert_query
    generated_id = insert_data_func(
        'insert into categories(name,is_private,is_locked) values(?,False,False)',
        (category.name,))

    category.id = generated_id

    return category

#A function to update a single category to being private
def make_private(category_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(category_id)
 
    if result:
        return  update_data_func('''UPDATE categories SET
           is_private = True where category_id = ?''', (result.id,))
    else:
        return NotFound("No such category exist!")

#A function to update a single category to being public
def make_public(category_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(category_id)
 
    if result:
        return  update_data_func('''UPDATE categories SET
           is_private = False where category_id = ?''', (result.id,))
    else:
        return NotFound("No such category exist!")

#A function to update a single category to being locked
def locked(category_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(category_id)
 
    if result:
        return  update_data_func('''UPDATE categories SET
           is_locked = True where category_id = ?''', (result.id,))
    else:
        return NotFound("No such category exist!")


#A function to update a single category to being unlocked
def unlock(category_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(category_id)
 
    if result:
        return  update_data_func('''UPDATE categories SET
           is_locked = False where category_id = ?''', (result.id,))
    else:
        return NotFound("No such category exist!")
