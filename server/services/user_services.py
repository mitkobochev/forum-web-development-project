from server.data import database
from server.data.models import Users, Role, Messages
from mariadb import IntegrityError
from server.common.response import NotFound

_SEPARATOR = ';'

# A function to retrieve the information on a user from the database, based on username
def find_by_username(username: str, get_data_func=None) -> Users | None:
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''SELECT user_id, username, password, user_role FROM users WHERE username = ?''',
        (username,))

    return next((Users.from_query_result(*row) for row in data), None)

# A function that returns user information of an already existing user
def try_login(username: str, password: str) -> Users | None:
    user = find_by_username(username)

    return user if user and user.password == password else None

# A function to create a user and store it into the database
def create(username: str, password: str, insert_data_func=None) -> Users | None:
    if insert_data_func is None:
        insert_data_func = database.insert_query
    try:
        generated_id = insert_data_func(
            'INSERT INTO users(username, password, user_role) VALUES (?,?,?)',
            (username, password, Role.MEMBER))

        return Users(id=generated_id, username=username, password='', user_role=Role.MEMBER)

    except IntegrityError:
        return None

# A function to create a token that will be used in user login
def create_token(user: Users) -> str:
    return f'{user.id}{_SEPARATOR}{user.username}'

# A function that checks if the user is already registered and split the token to be ready for use
def is_authenticated(token: str, get_data_func=None) -> bool:
    if get_data_func is None:
        get_data_func = database.read_query
    return any(get_data_func(
        'SELECT 1 FROM users where user_id = ? and username = ?',
        token.split(_SEPARATOR)))


def from_token(token: str) -> Users | None:
    _, username = token.split(_SEPARATOR)
    return find_by_username(username)

# A function that check if a user has existing messages
def has_message(user: Users, message: Messages) -> bool:
    return message.sender_id == user.id

# A function that promotes an existing user to a category member
def promote_to_category_member(username: str, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = find_by_username(username)
    if result:
        return  update_data_func('''UPDATE users SET
           user_role = 'category_member' where username = ?''', (result.username,))
    else:
        return NotFound("User does not exists or it's already promoted!")

# A function that demotes an existing user back to a member
def demote(username: str, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = find_by_username(username)
    if result:
        return  update_data_func('''UPDATE users SET
           user_role = 'member' where username = ?''', (result.username,))
    else:
        return NotFound("User does not exists!")

# A function that retrieves all users that are category members and their information, except their passwords
def access(get_data_func =None):
    if get_data_func is None:
            get_data_func = database.read_query
    data = get_data_func('''Select user_id, username,password = 'hidden', user_role from users where user_role = "category_member"''')

    return (Users.from_query_result(*row) for row in data)