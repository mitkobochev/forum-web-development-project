from server.common.response import NotFound
from server.data import database
from fastapi import  Header
from server.data.models import Messages, Users
from server.common.auth import get_user_or_raise_401

#A function to retrieve the entirety of messsages from the database
def all(search: str = None, get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    if search is None:
        data = get_data_func(
            '''SELECT m.message_id, m.text, us.username, u.username
                from messages as m
                join users as u on m.reciever_id = u.user_id
                join users as us on m.sender_id = us.user_id order by message_id
                ''')
    else:
       data = get_data_func(
            '''SELECT m.message_id, m.text,us.username, u.username
                from messages as m
                join users as u on m.reciever_id = u.user_id
                join users as us on m.sender_id = us.user_id
               WHERE text LIKE ?''', (f'%{search}%',))
               
    return (Messages.from_query_result(*row) for row in data)

#A function to retrieve a single message from the database
def get_by_id(message_id: int | str, get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    
    message_data = get_data_func(
        '''select message_id, text, sender_id, reciever_id from messages
            where message_id = ?''', (message_id,))
    if message_data == []:
        return NotFound("No messages found!")

    return next((Messages.from_query_result(*row) for row in message_data), None)

#A function to retrieve the entirety of messsages from a single user
def get_message_by_user_id(sender_id: int | str, get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query
    
    message_data = get_data_func(
        '''select message_id, text, us.username, u.username from messages as m 
            join users as u on m.reciever_id = u.user_id
            join users as us on m.sender_id = us.user_id
            where sender_id = ?''', (sender_id,))
    if message_data == []:
        return NotFound("No messages found!")

    return (Messages.from_query_result(*row) for row in message_data)

#A function to retrieve the entirety of messsages from one user to another
def view_conversation(sender_id: int | str, reciever_id: int | str, get_data_func = None):
    if get_data_func is None:
        get_data_func = database.read_query


    conversation_data = get_data_func(
        '''select message_id, text, us.username, u.username from messages as m
        join users as u on m.reciever_id = u.user_id
        join users as us on m.sender_id = us.user_id
        where m.sender_id = ? and m.reciever_id = ? or m.reciever_id = ? and m.sender_id = ?
        ''', (sender_id,reciever_id,sender_id,reciever_id,))
        
    return (Messages.from_query_result(*row) for row in conversation_data)

#A function to create a messsage and store it into the database
def create_message(message: Messages, insert_data_func = None):
    if insert_data_func is None:
        insert_data_func = database.insert_query
    generated_id = insert_data_func(
        '''INSERT INTO messages(text, sender_id, reciever_id) VALUES(?,?,?)''',
        (message.text, message.sender_id, message.reciever_id))

    data = database.read_query(
            '''SELECT m.message_id, m.text,us.username, u.username
                from messages as m
                join users as u on m.reciever_id = u.user_id
                join users as us on m.sender_id = us.user_id
               WHERE message_id LIKE ?''', (f'%{generated_id}%',))

    return (Messages.from_query_result(*row) for row in data)

#A function to update a messsage's text and store it into the database
def update(old: Messages, new: Messages, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query
    merged = Messages(
        id=old.id,
        text= new.text or old.text,
        sender_id=old.sender_id,
        reciever_id=old.reciever_id)
    update_data_func(
        '''UPDATE messages SET
           text = ?
           WHERE message_id = ? 
        ''',
        (merged.text, merged.id))
        
    return (merged)

#A function to delete a messsage from the database
def delete_message(message: Messages,delete_data_func = None):
    parse_int_to_list =[]
    parse_int_to_list.append(message.id)
    if delete_data_func is None:
        delete_data_func = database.update_query
    delete_data_func('DELETE FROM messages where message_id = ?',(parse_int_to_list))


