from server.data import database
from server.data.models import Category, Topics
from server.common.response import NotFound, Unauthorized


#A function to retrieve the entirety of topics from the database
def all(search: str = None, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    if search is None:
        data = get_data_func(
            '''SELECT topic_id, title, description, creation_date, author_id, c.category_id
         from topics as t join categories as c on t.category_id = c.category_id where is_private = 0 or is_private = 1''')
    else:
        data = get_data_func(
            '''SELECT topic_id, title, description, creation_date, author_id, c.category_id
         from topics as t join categories as c on t.category_id = c.category_id where is_private = 0 or is_private = 1 
         and title LIKE ?''', (f'%{search}%',))

    return (Topics.from_query_result(*row) for row in data)

#A function to retrieve the entirety of topics from the database, based on the fact that the topic is non-private
def public(search: str = None, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    if search is None:
        data = get_data_func(
            '''SELECT topic_id, title, description, creation_date, author_id, c.category_id
         from topics as t join categories as c on t.category_id = c.category_id where is_private = 0''')
    else:
        data = get_data_func(
            '''SELECT topic_id, title, description, creation_date, author_id, c.category_id
         from topics as t join categories as c on t.category_id = c.category_id where is_private = 0 and title LIKE ?''', (f'%{search}%',))

    return (Topics.from_query_result(*row) for row in data)

#A function to retrieve a topic from the database
def get_by_id(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''SELECT topic_id, title, description, creation_date, author_id, category_id
               FROM topics 
            WHERE topic_id = ?''', (id,))

    return next((Topics.from_query_result(*row) for row in data), None)

#A function to retrieve a topic from the database, based on category ID
def get_topic_by_category(category_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''SELECT topic_id, title, description, creation_date, author_id, category_id
               FROM topics 
            WHERE category_id = ?''', (category_id,))

    return (Topics.from_query_result(*row) for row in data)

#A function to sort the existing topics by id, title and creation dates
def sort(topics: list[Topics], *, attribute='creation_date', reverse=False):
    if attribute == 'creation_date':
        def sort_fn(t: Topics): return t.creation_date
    elif attribute == 'title':
        def sort_fn(t: Topics): return t.title
    else:
        def sort_fn(t: Topics): return t.id

    return sorted(topics, key=sort_fn, reverse=reverse)

#A function to create a topic and store it in the database
def create(topic: Topics, insert_data_func=None):
    if insert_data_func is None:
        insert_data_func = database.insert_query

    generated_id = insert_data_func(
        '''INSERT INTO topics(title,description,creation_date,author_id, category_id) VALUES(?,?,?,?,?)''',
        (topic.title, topic.description, topic.creation_date, topic.author_id, topic.category_id))

    topic.id = generated_id

    return topic

#A function to check if a topic is private
def check_privacy(get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query

    get_private = get_data_func(
        '''select category_id from categories where is_private =1''')

    return (Category(id=id) for id in get_private)

#A function to check if a topic exists in the database
def exist(id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    return any(get_data_func('''SELECT topic_id, title, description, creation_date, author_id, category_id
               FROM topics 
            WHERE topic_id = ?''', (id,)))

#A function to update the title and description of a topic
def update(old: Topics, new: Topics, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    merged = Topics(
        id=old.id,
        title=new.title or old.title,
        description=new.description or old.description,
        creation_date=old.creation_date,
        author_id=old.author_id,
        category_id=old.category_id
    )
    update_data_func(
        '''UPDATE topics SET
           title = ?, description = ?
           WHERE topic_id = ? 
        ''',
        (merged.title, merged.description, merged.id))
    return merged

#A function to retrieve the entirety of topics from the database, based on topic ID
def return_all_topics_by_id(topic_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''select topic_id, title, description, creation_date, author_id, t.category_id from topics as t 
        right join categories as c on c.category_id = t.category_id 
        WHERE topic_id = ? or c.is_private = 0 and c.is_private = 1;''', (topic_id,))

    return (Topics.from_query_result(*row) for row in data)

#A function to retrieve the entirety of topics from the database, based on topic ID and if the topics are non-private
def return_restricted_topics_by_id(topic_id: int, get_data_func=None):
    if get_data_func is None:
        get_data_func = database.read_query
    data = get_data_func(
        '''select topic_id, title, description, creation_date, author_id, t.category_id from topics as t 
        right join categories as c on c.category_id = t.category_id 
        WHERE topic_id = ? and c.is_private = 0;''', (topic_id,))
    if data == []:
        return Unauthorized("You cannot view this private topic!")
    return (Topics.from_query_result(*row) for row in data)

#A function to lock a topic, based on topic ID
def lock_topic(topic_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(topic_id)
 
    if result:
        return  update_data_func('''UPDATE topics SET
           is_locked = True where topic_id = ?''', (result.id,))
    else:
        return NotFound("No such topic exist!")

#A function to unlock a topic, based on topic ID
def unlock_topic(topic_id: int, update_data_func=None):
    if update_data_func is None:
        update_data_func = database.update_query

    result = get_by_id(topic_id)
 
    if result:
        return  update_data_func('''UPDATE topics SET
           is_locked = False where topic_id = ?''', (result.id,))
    else:
        return NotFound("No such topic exist!")