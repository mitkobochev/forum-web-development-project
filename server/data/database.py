from mariadb import connect
from mariadb.connections import Connection

# Function that establishes a connection with the database
def _get_connection() -> Connection:
    return connect(
        user='root',
        password='997788',
        host='localhost',
        port=3306,
        database='project_file_db'
    )

# A function that helps with the insertion of new data into the database
def insert_query(sql: str, sql_params=()) -> int:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid

# A function that assists with the pulling of information from the database to be used in Postman  
def read_query(sql: str, sql_params=()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return list(cursor)

# A function that helps with the updating of already existing information to the database
def update_query(sql: str, sql_params=()) -> bool:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount
