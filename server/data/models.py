from datetime import date
from pydantic import BaseModel, constr

TUsername = constr(regex='^\w{2,30}$') #username restrictions


class Role:
    MEMBER = 'member'
    ADMIN = 'admin'
    CATEGORY_MEMBER = "category_member"

class Category(BaseModel):
    id: int | None
    name: str
    is_private: bool | None
    is_locked: bool | None

    def private(self):
        return self.is_private == True
    
class Messages(BaseModel):
    id: int | None
    text: str
    sender_id: str | int
    reciever_id: str | int

    @classmethod
    def from_query_result(cls, id, text, sender_id, reciever_id):
        return cls(
            id=id,
            text=text,
            sender_id=sender_id,
            reciever_id=reciever_id)

class Users(BaseModel):
    id: int | None | str
    username: str
    password: str
    user_role: str

    def is_admin(self):
        return self.user_role == Role.ADMIN

    def is_category_member(self):
        return self.user_role == Role.CATEGORY_MEMBER

    @classmethod
    def from_query_result(cls, id, username, password, user_role):
        return cls(
            id=id,
            username=username,
            password=password,
            user_role=user_role)
        
class LoginData(BaseModel):
    username: TUsername
    password: str

class Topics(BaseModel):
    id: int | None
    title: str
    description: str
    creation_date: date
    author_id: int
    category_id: int

    @classmethod
    def from_query_result(cls, id, title, description, creation_date,author_id, category_id):
        return cls(
            id=id,
            title=title,
            description=description,
            creation_date=creation_date,
            author_id=author_id,
            category_id=category_id)

class Replies(BaseModel):
    id: int | None
    text: str
    creation_date: date
    votes:int | None
    topic_id: int
    user_id: int
    is_best_reply : bool | None
    @classmethod
    def from_query_result(cls, id, text, creation_date, votes, topic_id,user_id,is_best_reply):
        return cls(
            id=id,
            text=text,
            creation_date=creation_date,
            votes=votes,
            topic_id=topic_id,
            user_id=user_id,
            is_best_reply=is_best_reply)

class UserVoteModel(BaseModel):
    vote_id: int | None
    user_id: int
    reply_id: int
    vote: int

    @classmethod
    def from_query_result(cls,vote_id, user_id, reply_id, vote):
        return cls(
            vote_id=vote_id,
            user_id=user_id,
            reply_id=reply_id,
            vote=vote)